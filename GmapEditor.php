<?php
/**
 * Author: Mehdi Achour
 */

namespace yiidk\gmapeditor;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;

/**
 * Class Instafeed
 *
 * @package yiidk\gmapeditor
 */
class GmapEditor extends Widget
{

    /**
     * Widget options
     *
     * ```php
     * ```
     *
     * @var array {@link http://gmapeditorjs.com/ Instafeed options}
     */
    public $options = [
        'lat'  => '',
        'lng' => '',
    ];

    public $form;
    public $model;

    /**
     * Container tag name
     *
     * @var string
     */
    public $tagName = 'div';

    /**
     * Html options of the container tag
     *
     * @var array
     */
    public $htmlOptions = [ 
        'class' => 'form-group',
        'style' => 'width: 600px; height: 400px;',
    ];

    /**
     * Initializes the widget.
     * This renders the open tag.
     */
    public function init()
    {
        parent::init();

        if (empty($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = $this->id;
        }

        $this->options['target'] = $this->htmlOptions['id'];

        echo $this->form->field($this->model, $this->options['lng'], ['template' => '{input}{error}'])->hiddenInput();
        echo $this->form->field($this->model, $this->options['lat'], ['template' => '{input}{error}'])->hiddenInput();
        echo $this->form->field($this->model, $this->options['z'], ['template' => '{input}{error}'])->hiddenInput();
        echo Html::beginTag($this->tagName, $this->htmlOptions) . "\n";
    }

    /**
     * Runs the widget.
     * This registers the necessary javascript code and renders the close tag.
     */
    public function run()
    {
        parent::run();

        $options = empty($this->options) ? '' : Json::encode($this->options);
        $id = $this->htmlOptions['id'];
        $js = <<<EOD
var myOptions = {
        zoom: 6,
        mapTypeId: google.maps.MapTypeId.HYBRID,
        streetViewControl: false,
        center: new google.maps.LatLng(35, 10.31),
        optimized: false
    };
var map = new google.maps.Map(document.getElementById("$id"), myOptions);


var marker = new google.maps.Marker();

new google.maps.event.addListener(map, 'click', function(a) {
    $('[id$=-{$this->options['lat']}]').val(a.latLng.lat().toString());
    $('[id$=-{$this->options['lng']}]').val(a.latLng.lng().toString());
    $('[id$=-{$this->options['z']}]').val(map.getZoom().toString());
    setTimeout("$('[id$=-{$this->options['lat']}]').change()", 100);
});

$('[id$=-{$this->options['lat']}], [id$=-{$this->options['lng']}]').bind('input change', function() {
    var lat = $('[id$=-{$this->options['lat']}]').val(),
        lng = $('[id$=-{$this->options['lng']}]').val(),
        reg = /^\s*\d+\.?\d+\s*$/;

    if (reg.test(lat) && reg.test(lng)) {
        marker.setPosition( new google.maps.LatLng($('[id$=-{$this->options['lat']}]').val(), $('[id$=-{$this->options['lng']}]').val()) )
        marker.setMap(map);
    } else {
        marker.setMap(null);
    }

})

$('[id$=-{$this->options['lat']}]').change();
EOD;

        $view = $this->getView();
        $view->registerJs($js);

        GmapEditorAsset::register($view);

        echo Html::endTag($this->tagName);
    }
} 

