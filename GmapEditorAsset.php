<?php

namespace yiidk\gmapeditor;

use yii\web\AssetBundle;

/**
 * Class InstafeedAsset
 *
 * @package yiidk\gmapeditor
 */
class GmapEditorAsset extends AssetBundle
{
    public $js = [
        'http://maps.google.com/maps/api/js?sensor=false',
    ];

} 
